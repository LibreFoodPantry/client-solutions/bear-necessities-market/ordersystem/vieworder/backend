const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const PORT = 4000;
const mongoPort = "mongo";

app.use(cors());
app.use(bodyParser.json());

//Default Route
app.get('/api', (req, res) => {
  res.status(200).send('View Order API')
});

//Connection to the Database
mongoose.connect('mongodb://'.concat(mongoPort).concat(':27017/Orders'), { useNewUrlParser: true, useUnifiedTopology: true});
const connection = mongoose.connection;
connection.once('open', function(){
    console.log("MongoDB connection established");
})

app.listen(PORT, function(){
    console.log("Server is running on port:" + PORT);
});
