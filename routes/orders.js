const express = require('express');
const router = express.Router();
const Order = require('../models/Order');

//Post Request(s)
router.post('/post', async (req, res) => {
    const order = new Order({
        orderType: req.body.orderType,
        itemsOrdered: req.body.itemsOrdered,
        dietaryRestrictions: req.body.dietaryRestrictions,
        commentsPreferences: req.body.commentsPreferences,
        email: req.body.email,
        emailVerified: req.body.emailVerified
    });
    try{
        const savedOrder = await order.save();
        res.status(201).json(savedOrder);
    }catch(err){
        res.status(204).json({message: err});
    }
});

//Get Request(s)
router.get('/get', async (req, res) => {
    try{
        const orders = await Order.find();
        res.status(200).json(orders);
    }catch(err){
        res.status(404).json({message:err});
    }
});

router.get('/get/:orderID', async (req, res) => {
    try{
        const specificorder = await Order.findById(req.params.orderID);
        res.status(200).json(specificorder);
    }catch(err){
        res.status(404).json({message:err});
    }
});

router.get('/get/email/:emailID', async (req, res) => {
    try{
        const orders = await Order.find({"email": req.params.emailID.toLowerCase()});
        res.status(200).json(orders);
    }catch(err){
        res.status(404).json({message:err});
    }
});

router.get('/get/date/:orderDate', async (req, res) => {
    try{
        const orders = await Order.find({"dateOrdered": req.params.orderDate});
        res.status(200).json(orders);
    }catch(err){
        res.status(404).json({message:err});
    }
});

//Delete Request(s)
router.delete('/delete/:orderID', async (req, res) => {
    try{
        const specificorder = await Order.findByIdAndDelete(req.params.orderID);
        res.status(200).json(specificorder);
    }catch(err){
        res.status(404).json({message:err});
    }
});

module.exports = router;