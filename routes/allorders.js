const express = require('express');
const router = express.Router();
const Order = require('../models/Order');


//Get Request(s)
router.get('/get', async (req, res) => {
    try{
        const orders = await Order.find();
        res.status(200).json(orders);
    }catch(err){
        res.status(404).json({message:err});
    }
});


module.exports = router;
