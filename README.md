## Purpose

  

This repository will house all the technology used to construct a functional backend for the ViewOrder component.
In other words this project will contain the behind the scenes code needed for viewing Bear Necessities Market orders.



## Tech Stack

  

Backend: Node.js, Docker, JavaScript, MongoDB

  

Node.js - A JavaScript runtime environment that will serve as the basis for servers on this component and also function as a middle-ware between the ViewOrder's frontend and backend.

  

Docker - Tool that will be used to containerize the project in order to ensure independence and minimize any risk to impacting the functionality of other projects.

  

JavaScript - Primary language utilized for frontend and backend development.

  

MongoDB - NoSQL database that uses a JSON format to store objects.

  

## Quick Guide

  

### Starting Up Backend


If you are not on the latest branch you will need to follow the update guide for 'docker-compose up' to work.


Preface: Docker will have to be installed prior to beginning any new development on ViewOrder’s backend or for the process of starting up the backend.

  

If Docker is not installed on your machine please refer to this [link](https://www.docker.com/get-started) to download Docker.

  

Please refer to the below instructions if Docker is already installed:

  

1. Clone the Backend project inside the ViewOrder subgroup to your machine

2. Navigate to the cloned backend folder

3. Open a Command-line interface (CLI) within the directory

4. Type in the `docker-compose up` command to start up the project

  

Once the `docker-compose up` command has been executed the Docker image is ready to build the local image, spin up the other services as required, and start up the containers. 

You can verifiy that the server is running by typing 'http://localhost:4000/api/' in your browser of choice

  
### Manual Update Instructions

Some updates are required to get 'Docker-compose up' to work properly

1. Open Dockerfile and remove the three comments (#) in the file (lines 6, 7, and 9)

  (macOS) Replace node:8.7.0-alpine to node:16.1.0-alpine on line 1

2. Open docker-compose.yml, on line 8 replace dockerfile_backend_server with Dockerfile.

3. Change Mongodb on lines 13, 16 and 17 to mongo

4. Move lines 20 and 21 to line 14 and 15, placing them after mongo but before networks

20

&nbsp;&nbsp;&nbsp;&nbsp;volumes:

21

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\- ./data:/data/db

5. Open package.json and add two lines before "express": "^4.17.1" 

(The quotation marks may be changed if you copy and paste them)

&nbsp;&nbsp;&nbsp;&nbsp;"cors": "^2.8.5",

&nbsp;&nbsp;&nbsp;&nbsp;"mongoose": "^5.9.7",
    
7. Open server.js and replace line 18 with 

mongoose.connect('mongodb://'.concat(mongoPort).concat(':27017/Orders'), { useNewUrlParser: true, useUnifiedTopology: true});



### Shutting Down the Backend

  

1. Navigate to backend

2. Open a Command-line interface (CLI) within the directory

3. Type in the `docker-compose down` command to shut down the container

  
  

## Current Status

  As it stands the ViewOrder backend has much of the foundation complete including having a simple Docker install and a barebones node.js project; MongoDB also works but it should be updated to work better with PlaceOrder's backend.
There is a template code quality test that should be replaced with a Nodejs template when ViewOrder is on a web server.
  

## Files Details

  

Models – Holds schemas required for MongoDB

  

Routes – Holds API logic

 

EsLint – JavaScript Linter

  

Package/Package-lock.json – Holds dependencies