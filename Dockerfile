FROM node:16.1.0-alpine

RUN mkdir -p /srv/app/Backend
WORKDIR /srv/app/Backend

COPY package.json /srv/app/Backend
COPY package-lock.json /srv/app/Backend

RUN npm install --silent

COPY . /srv/app/Backend

EXPOSE 4000
EXPOSE 8080

CMD ["node", "server.js"]
